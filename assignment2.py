from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

# @app.route('/book/json')
# def bookjson():
# 	r = json.dumps(books)
# 	return Response(r)

@app.route('/')
@app.route('/book/')
def showBook():
    # <your code>
	return render_template('showBook.html', books=books)

@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    # <your code>
	if request.method=='POST':
		new_book = request.form['new_book']
		books.append({'title':new_book, 'id':str(len(books)+1)})
		return redirect(url_for('showBook'))
	else:
		return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    # <your code>
	if request.method=='POST':
		new_name = request.form['new_title']
		for i in range(len(books)):
			if books[i]['id'] == str(book_id):
				books[i]['title'] = new_name
				break
		return redirect(url_for('showBook'))
	else:
		return render_template('editBook.html', book_id=book_id)

@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    # <your code>
	if request.method=='POST':
		i = 0
		found = False
		while i < len(books):
			if books[i]['id'] == str(book_id):
				books.pop(i)
				i -= 1
				found = True
			elif found:
				books[i]['id'] = str(int(books[i]['id']) - 1)
			i += 1
		return redirect(url_for('showBook'))
	else:
		return render_template('deleteBook.html', books=books, book_id=book_id)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
